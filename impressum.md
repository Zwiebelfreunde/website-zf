---
layout: page
title: Impressum
permalink: /impressum/
redirect_from: /contact.html
---
Zwiebelfreunde e.V.   
c/o DID Dresdner Institut für Datenschutz   
Palaisplatz 3   
D-01097 Dresden   
Telefon/Signal: +49 (0) 172 63 43 48 0   


Amtsgericht Dresden, VR 5388.   
Vertretungsberechtiger Vorstand: Moritz Bartl, Juris Vetra, Jens Kubieziel.