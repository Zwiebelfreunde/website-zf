---
layout: home
---
## Über uns

{: style="text-align: justify" }
Der Zwiebelfreunde e.V. ist ein Verein, der Technologie und Wissen im Bereich Anonymität, Privatsphäre und Sicherheit im Internet fördert, zur Nutzung bereit stellt und deren Verbreitung unterstützt. Der Zwiebelfreunde e.V. betreibt unter anderem das erfolgreiche [Torservers.net](https://torservers.net) Projekt.   
Unser Ziel ist es die Öffentlichkeit über die zugrunde liegenden Themen Privatsphäre, Anonymität, Sicherheit, sowie Redefreiheit aufzuklären. Nicht zuletzt sind wir ein verlässlicher Ansprechpartner für Presse und Behörden.

## Projekte und Aktivitäten

   * Wir betreiben Tor-Knoten in allen möglichen Formen und Größen. Dazu benötigt es fähiger Administratoren, die sich um den Betrieb kümmern, guter Organisatoren, die sich um Förderanträge und den Kontakt mit Hostern kümmern, Pressemeldungen schreiben, etc...
   * Wir geben Interviews zu Themen wie Anonymität im Internet, Tor, dem (angeblichen) "Darknet" und verwandten Themen.
   * Wir beraten Organisationen und Behörden zum Einsatz von sicheren Kommunikationsmitteln.
   * Wir sprechen auf Konferenzen über unsere Erfahrungen, halten Workshops und haben auch ein großes Interesse daran, lokale Nutzertreffen zu organisieren und zu unterstützen.